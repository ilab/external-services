class StepsController < ApplicationController

  def manifest
    default_steps = [:date, :pregnancy]
    @steps = params[:steps] || default_steps
    render :manifest, content_type: 'application/xml', layout: false
  end

  def date
    unit = params[:unit].downcase
    value = params[:value].to_i

    cal_date = value.send(unit.to_sym).ago.strftime("%d/%m/%Y")

    result = {result: cal_date }
    render xml: result

  end
end